import React  from 'react'
import { Switch, Route } from "react-router-dom";

import SignupForm from '../Form/SignupForm';
import Profile from '../Profile/ProfileView'
import ProductDetails from '../Products/ProductDetails';
import Products from '../Products/Products';

const AppRoutes = () => {
    return (
        <>
            <Switch>
				<Route exact path='/'>
					<SignupForm />
				</Route>

				<Route exact path='/profile'>
					<Profile />
				</Route>

				<Route exact path='/products'>
					<Products />
				</Route>

				<Route exact path='/productDetails'>
					<ProductDetails />
				</Route>
			</Switch>
        </>
    )
}

export default AppRoutes
