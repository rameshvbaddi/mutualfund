import React from 'react'

export default function Select({ options, value, placeholder, id, onChange, readOnly=false, ...rest }) {
    return (
        <>
            <select
            
                className='container-fluid rounded border p-2'
                placeholder={placeholder}
                value={value}
                id={id}
                onChange={(e) => onChange(e)}
                disabled={readOnly}
                {...rest}
            >
              {options.map((op, i) => {
                  return <option key={i}>{op}</option>
              })}
            </select>
        </>
    )
}
