import React from 'react'

export default function Input({value, placeholder, type, id, onChange, icon, ...rest}) {
    return (
        <>
            {icon && icon }
             <input
                className={'rounded border p-2 ' + (icon ? 'col-10': 'col-12')}
                placeholder={placeholder}
                type={type}
                value={value}
                id={id}
                onChange={(e) => onChange(e)}
                {...rest}
            />
        </>
    )
}
