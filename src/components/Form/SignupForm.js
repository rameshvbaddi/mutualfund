import React, { useState, useContext, useEffect } from 'react'

import {AppContext} from '../../AppContext/AppContext'
import './style.css'
import Input from '../commons/Input'
import Select from '../commons/Select'


const SignupForm = () => {
    
    const { authUser, updateUser, user } = useContext(AppContext)
    const [isValid, setIsValid] = useState(false)

    const genderOptions = ['Male', 'Female', 'Other']

    const validateForm = (val = true) => {
        if(val && user.name && user.email && user.password && user.age) {
            return setIsValid(true)
        }
        setIsValid(false)
    }

    const updateForm = (e) => {
        updateUser(e.target.id, e.target.value)
        validateForm(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        authUser(true)
    }
    
    useEffect(() => {
        validateForm()
    }, [user])


    return (
        <div className='container signUpForm rounded darkDrpShadow'>
            <div className='row justify-content-center border-bottom mt-2 p-4'>
                <h3 className=''>Sign up</h3>
            </div>
            <div className='row mt-4'>
                <form className='container text-right' onSubmit={e => handleSubmit(e)}>

                    <div className='row form-group justify-content-center '>
                        <div className='col-2 p-2'>Name</div>
                        <div className='col'>
                            <Input
                                placeholder='Enter name'
                                type='text'
                                value={user.name}
                                id='name'
                                onChange={updateForm}
                            />
                        </div>
                    </div>


                    <div className='row form-group justify-content-center'>
                        <div className='col-2 p-2'>Email</div>
                        <div className='col'>
                            <Input
                                placeholder='Enter email'
                                type='text'
                                value={user.email}
                                id='email'
                                onChange={updateForm}
                            />
                        </div>
                    </div>

                    <div className='row form-group justify-content-center'>
                        <div className='col-2 p-2'>Password</div>
                        <div className='col'>
                            <Input
                                placeholder='Enter password'
                                type='password'
                                value={user.password}
                                id='password'
                                onChange={updateForm}
                            />
                        </div>
                    </div>

                    <div className='row form-group justify-content-center'>
                        <div className='col-2 p-2'>User Type</div>
                        <div className='col'>
                            <Select
                                options={['Co-ordinator','Respondent']}
                                placeholder='Enter password'
                                value={user.userType}
                                id='userType'
                                onChange={updateForm}
                            />
                        </div>
                    </div>

                    <div className='row form-group justify-content-center'>
                        <div className='col-2 p-2'>Gender</div>
                        <div className='col-4'>
                            <Select 
                                id='gender'
                                placeholder='Select Gender'
                                value={user.gender}
                                options={genderOptions}
                                onChange={updateForm}
                            />
                        </div>

                        <div className='col-2 text-right p-2'>Age</div>
                        <div className='col-4'>
                            <Input
                                placeholder='Enter Age'
                                type='number'
                                value={user.age}
                                id='age'
                                onChange={updateForm}
                            />
                        </div>
                    </div>

                    <div className='row p-4 justify-content-center border-top'>
                        <button className='btn btn-md btn-primary col-lg-4' type='submit' disabled={!isValid}>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default SignupForm
