import React, { useContext } from 'react'

import './style.css'
import { AppContext } from '../../AppContext/AppContext'

const Header = () => {
    const {goToSignup, goToList, goToProfile, isAuth} = useContext(AppContext)

    return (
        <header className="header container-fluid p-3 lightDrpShadow">
            <div className='row'>
                <div className='col-3'><h3 onClick={goToSignup}>MUTUAL FUNDS</h3></div>
                { isAuth && <>
                    <div className='col justify-content-end text-right'>
                        <button className='btn btn-link mr-2 border-none' onClick={goToList}>Mutual funds</button>
                        <button className='btn btn-link mr-2' onClick={goToProfile}>My Profile</button>
                        <button className='btn btn-link' onClick={goToSignup}>Logout</button>
                    </div>
                </>}
                
            </div>
        </header>
    )
}


export default Header
