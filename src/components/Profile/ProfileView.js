import React, { useState, useContext } from 'react'
import './style.css'

import Input from '../commons/Input'
import Select from '../commons/Select'
import { AppContext } from '../../AppContext/AppContext'


const ProfileView = () => {

    //const src1 = https://photos.angel.co/users/8459038-large?1579626396
    const src = 'https://cdn2.vectorstock.com/i/thumb-large/23/81/default-avatar-profile-icon-vector-18942381.jpg'

    const { user, updateUser } = useContext(AppContext)
    const [isEdit, setIsEdit] = useState(false)

    const handleChange = (e) => {
        updateUser(e.target.id, e.target.value)
    }

    // const handleSelect = (e) => {
    //     console.log(e)
    // }

    const handleEdit = () => {
        setIsEdit(!isEdit)
    }

    return (
        <div className='container p-4 '>
            <div className='position-relative text-right'>
                <button className={'btn position-absolute editProfile ' + (isEdit ? 'btn-success' : 'btn-secondary') } onClick={() => handleEdit(!isEdit)}>{isEdit ? 'Save' : 'Edit'}</button>
            </div>
            <div className='row  bg-white pb-4 pt-4 mt-3 lightDrpShadow rounded'>
                <div className='col-2'>
                    <img className='lightDrpShadow rounded' src={src} height='100' width='100' alt="Profile" />
                </div>

                <div className='col-5 text-left'>
                    <div className='row'>
                        <div className='col-2 pt-3'>
                            Name
                        </div>  
                        <div className='col p-2'>
                            <Input
                                readOnly={!isEdit}
                                id='name'
                                value={user.name}
                                type='text'
                                onChange={(e) => handleChange(e)}
                            />
                        </div>  
                    </div>

                    <div className='row'>
                        <div className='col-2 p-3'>
                            Email
                        </div>  
                        <div className='col p-2'>
                            <Input
                                readOnly={!isEdit}
                                id='email'
                                value={user.email}
                                type='email'
                                onChange={(e) => handleChange(e)}
                            />
                        </div>  
                    </div>
                    <div className='row'>
                        <div className='col-2 p-3'>
                            Gender
                        </div>  
                        <div className='col p-2'>
                            <Select
                                readOnly={!isEdit}
                                id='gender'
                                placeholder='Select Gender'
                                value={user.gender}
                                options={['Male', 'Female', 'Other']}
                                onChange={(e) => handleChange(e)}
                            />
                        </div>  
                    </div>

                </div>

                <div className='col text-left'>
                    <div className='row'>
                        <div className='col-2 p-3'>
                            Age
                        </div>  
                        <div className='col p-2'>
                            <Input
                                readOnly={!isEdit}
                                id='age'
                                value={user.age}
                                type='number'
                                onChange={(e) => handleChange(e)}
                            />
                        </div>  
                    </div>
                    <div className='row'>
                        <div className='col-2 p-3'>
                            User
                        </div>  
                        <div className='col p-2'>
                            <Select
                                readOnly={!isEdit}
                                id='userType'
                                placeholder='Select Gender'
                                value={user.userType}
                                options={['Co-ordinator','Respondent']}
                                onChange={(e) => handleChange(e)}
                            />
                        </div>  
                    </div>
                    <div className='row'>
                        <div className='col-2 p-3'>
                            Password
                        </div>  
                        <div className='col p-2 text-gray'>
                            <Input
                                readOnly={!isEdit}
                                id='password'
                                value={user.password}
                                type='password'
                                onChange={(e) => handleChange(e)}
                            />
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProfileView