import React, { useState } from 'react'
import Input from '../commons/Input'
import { AppContext } from '../../AppContext/AppContext'

const Search = ({handleSearch}) => {

    const [search, setSearch] = useState('')

    const onSearch = (e) => {
        setSearch(e.target.value)
        handleSearch(e.target.value)
    }

    return (
        <div className='row justify-content-center mb-4'>
            <div className='col-5 text-center'>
                <Input 
                    icon={<span className="vAlignBtm material-icons pt-2 pb-2 pl-2 col-1 bg-dark text-white">search</span>}
                    type='text'
                    value={search}
                    placeholder='Search Mutual Fund by name'
                    onChange={(e) => onSearch(e)}
                />
            </div>
        </div>
    )
}

export default Search
