import React, { useState, useEffect } from 'react'
import {listData} from './mockData/listData'
import List from './List'
import Search from './Search'

const ListContainer = () => {

    const [data, setData] = useState([])
    const [pageNo, setPageNo] = useState(0)
    const [filterData, setFilterdData] = useState([])
 
    useEffect(() => {
        setData([...data, ...listData])
    }, [pageNo])

    const handleSearch = async (val) => {
        const newData = []
        
        await listData.map((m, i) => {
            if(m.fund_name.toLowerCase().search(val.toLowerCase()) == 0) {
                newData.push(m)
            }
        })
        newData.length ? setFilterdData(newData) : setFilterdData([])
    }

    console.log(filterData)

    return (
        <div className='row bg-white mt-5 lightShadow rounded'>
            <div className='col-12 pt-4'>
                <h5>Trending Mutual Funds</h5>
            </div>
            
            <div className='col-12 listContainer text-center p-4'>
                <Search handleSearch={handleSearch}/>
                {data.length
                    ? <List data={filterData.length ? filterData : data}/>
                    : <p className='text-center'>No mutual funds.</p>
                }
            </div>

            <div className='col-12 mb-4'>
                <button className='btn btn-info' onClick={() => setPageNo(pageNo + 1)}>Load More</button>
            </div>
        </div>
    )
}

export default ListContainer
