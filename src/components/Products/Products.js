import React from 'react'
import ListContainer from './ListContainer'

const Products = () => {
    return (
        <div className='container'>
            <ListContainer />
        </div>
    )
}

export default Products