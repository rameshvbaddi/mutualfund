import React, {useState} from 'react'

const pros = [
    'Risk is lower compared to the benchmark',
    'Lower expense ratio',
    '1Y Returns are higher than the benchmark',
    '3Y Returns are higher than the benchmark',
    '5Y Returns are higher than the benchmark',
    'Exit load is zero'
]

const cons = [
    'Risk-adjusted returns are lower compared to the category',
    '1Y Returns are lower than the benchmark',
    '3Y Returns are lower than the benchmark',
    '5Y Returns are lower than the benchmark'
]

const TabsContainer = () => {

    const [tab, setTab] = useState(1)

    return (
        <div className='row bg-white lightShadow'>
            <div className={'col-6 border-top p-3 cursorPointer ' + (tab == 1 ? 'border-right' : 'bg-light border-bottom lightText')} onClick={() => setTab(1)}>
                <h5>Pros</h5>
            </div>

            <div className={'col-6 border-top p-3 cursorPointer ' + (tab == 2 ? 'border-left' : 'bg-light border-bottom lightText')} onClick={() => setTab(2)}>
                <h5>Cons</h5>
            </div>

            {
                tab == 1 
                ? <div className='col-12 p-4 text-left'>
                    <ul className="list-group list-group-flush">
                        { pros.map((p, i) => <li key={i} className="list-group-item">{p}</li>) }
                    </ul>
                </div>
                : <div className='col-12 p-4 text-left'>
                    <ul className="list-group list-group-flush">
                        { cons.map((c, i) => <li key={i} className="list-group-item">{c}</li>) }
                    </ul>
                </div>
            }
        </div>
    )
}

export default TabsContainer
