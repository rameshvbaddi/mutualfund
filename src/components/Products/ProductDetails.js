import React, { useContext } from 'react'
import LineChartComp from './Charts/LineChart'
import { AppContext } from '../../AppContext/AppContext'
import TabsContainer from './Tabs/TabsContainer'
import PieChart from './Charts/PieChart'
import { data, debtData, holdingData } from './mockData/pieData'
import FundDetails from './FundDetails'

const ProductDetails = () => {

    const {lineChartData} = useContext(AppContext)

    return (
        <div className=' container-fluid p-4 '>
            <div className='row pr-4 pl-4'>
                <div className='chartConatiner col-8 col-sm-12 col-lg-8 bg-white p-5 lightShadow text-left mr-4'>
                    <LineChartComp lineChartData={lineChartData}/>
                </div>
                <div className='col bg-white lightShadow rounded'>
                    <PieChart data={data} title='Asset Allocation'/>
                </div>
            </div>

            <div className='row pr-4 pl-4 pt-4 '>
                <div className='col-4 mr-4 col-sm-12 col-lg-4'>
                    <TabsContainer />
                </div>
                <div className='col mr-4 lightShadow col-sm-12 col-lg-4'>
                    <PieChart data={holdingData} title='Holding Analysis'/>
                </div>
                <div className='col lightShadow col-sm-12 col-lg'>
                    <PieChart data={debtData} title='Debt Sector Allocation'/>
                </div>
            </div>

            <div className='row mt-4 text-left'>
                <div className='container-fluid'>
                    <FundDetails />
                </div>
            </div>
        </div>
    )
}

export default ProductDetails
