export const listData = [
{
    "id": "axis-equity-fund-direct-growth",
    "fund_name": "Axis Bluechip Fund",
    "search_id": "axis-equity-fund-direct-growth",
    "category": "Equity",
    "sub_category": "Large Cap",
    "sub_sub_category": "",
    "aum": 12716.8144,
    "available_for_investment": 1,
    "min_sip_investment": 500,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 8.25,
    "return1y": -8.6,
    "return5y": null,
    "nav": null,
    "return1d": 3,
    "min_investment_amount": 5000,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Axis Bluechip Fund Direct Plan Growth",
    "scheme_type": "Growth",
    "fund_manager": "Shreyash Devalkar",
    "fund_house": "Axis Mutual Fund",
    "scheme_code": "120465",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 45927,
    "direct_fund": "Axis Mutual Fund",
    "amc": "AXIS",
    "enable": null,
    "direct_search_id": "axis-equity-fund-direct-growth",
    "direct_scheme_name": "Axis Bluechip Fund",
    "term_page_view": 4113,
    "logo_url": "https://groww.in/mf-assets/logos/axis_groww.png"
}, {
    "id": "parag-parikh-long-term-value-fund-direct-growth",
    "fund_name": "Parag Parikh Long Term Equity Fund",
    "search_id": "parag-parikh-long-term-value-fund-direct-growth",
    "category": "Equity",
    "sub_category": "Multi Cap",
    "sub_sub_category": "",
    "aum": 2925.4304,
    "available_for_investment": 1,
    "min_sip_investment": 1000,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 7.3,
    "return1y": -2.16,
    "return5y": null,
    "nav": null,
    "return1d": -0.51,
    "min_investment_amount": 1000,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Parag Parikh Long Term Equity Fund Direct Growth",
    "scheme_type": "Growth",
    "fund_manager": "Raunak Onkar, Rajeev Thakkar, Raj Mehta",
    "fund_house": "PPFAS Mutual Fund",
    "scheme_code": "122639",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": true,
    "plan_type": "Direct",
    "page_view": 19102,
    "direct_fund": "PPFAS Mutual Fund",
    "amc": "PPFAS",
    "enable": null,
    "direct_search_id": "parag-parikh-long-term-value-fund-direct-growth",
    "direct_scheme_name": "Parag Parikh Long Term Equity Fund",
    "term_page_view": 1481,
    "logo_url": "https://groww.in/mf-assets/logos/ppfas_groww.png"
}, {
    "id": "axis-long-term-equity-fund-direct-growth",
    "fund_name": "Axis Long Term Equity Fund",
    "search_id": "axis-long-term-equity-fund-direct-growth",
    "category": "Equity",
    "sub_category": "ELSS",
    "sub_sub_category": "",
    "aum": 19632.0086,
    "available_for_investment": 1,
    "min_sip_investment": 500,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 4.77,
    "return1y": -11.02,
    "return5y": null,
    "nav": null,
    "return1d": -1.05,
    "min_investment_amount": 500,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Axis Long Term Equity Direct Plan Growth",
    "scheme_type": "Growth",
    "fund_manager": "Jinesh Gopani",
    "fund_house": "Axis Mutual Fund",
    "scheme_code": "120503",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 30889,
    "direct_fund": "Axis Mutual Fund",
    "amc": "AXIS",
    "enable": null,
    "direct_search_id": "axis-long-term-equity-fund-direct-growth",
    "direct_scheme_name": "Axis Long Term Equity Fund",
    "term_page_view": 1040,
    "logo_url": "https://groww.in/mf-assets/logos/axis_groww.png"
}, {
    "id": "mirae-asset-emerging-bluechip-fund-direct-growth",
    "fund_name": "Mirae Asset Emerging Bluechip Fund",
    "search_id": "mirae-asset-emerging-bluechip-fund-direct-growth",
    "category": "Equity",
    "sub_category": "Large & Mid Cap",
    "sub_sub_category": "",
    "aum": 8838.97584765,
    "available_for_investment": 1,
    "min_sip_investment": 1000,
    "sip_allowed": true,
    "lumpsum_allowed": false,
    "return3y": 1.25,
    "return1y": -14.13,
    "return5y": null,
    "nav": null,
    "return1d": 1.5,
    "min_investment_amount": 100,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Mirae Asset Emerging Bluechip Fund Direct Growth",
    "scheme_type": "Growth",
    "fund_manager": "Neelesh Surana",
    "fund_house": "Mirae Asset Mutual Fund",
    "scheme_code": "118834",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 90789,
    "direct_fund": "Mirae Asset Mutual Fund",
    "amc": "MIRAE_ASSET",
    "enable": null,
    "direct_search_id": "mirae-asset-emerging-bluechip-fund-direct-growth",
    "direct_scheme_name": "Mirae Asset Emerging Bluechip Fund",
    "term_page_view": 740,
    "logo_url": "https://groww.in/mf-assets/logos/mirae_groww.png"
},{
    "id": "axis-equity-fund-direct-growth",
    "fund_name": "Axis Bluechip Fund",
    "search_id": "axis-equity-fund-direct-growth",
    "category": "Equity",
    "sub_category": "Large Cap",
    "sub_sub_category": "",
    "aum": 12716.8144,
    "available_for_investment": 1,
    "min_sip_investment": 500.0,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 8.26,
    "return1y": -8.6,
    "return5y": null,
    "nav": null,
    "return1d": -0.52,
    "min_investment_amount": 5000.0,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Axis Bluechip Fund Direct Plan Growth",
    "scheme_type": "Growth",
    "fund_manager": "Shreyash Devalkar",
    "fund_house": "Axis Mutual Fund",
    "scheme_code": "120465",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 45927,
    "direct_fund": "Axis Mutual Fund",
    "amc": "AXIS",
    "enable": null,
    "direct_search_id": "axis-equity-fund-direct-growth",
    "direct_scheme_name": "Axis Bluechip Fund",
    "term_page_view": 4113,
    "logo_url": "https://groww.in/mf-assets/logos/axis_groww.png"
}, {
    "id": "parag-parikh-long-term-value-fund-direct-growth",
    "fund_name": "Parag Parikh Long Term Equity Fund",
    "search_id": "parag-parikh-long-term-value-fund-direct-growth",
    "category": "Equity",
    "sub_category": "Multi Cap",
    "sub_sub_category": "",
    "aum": 2925.4304,
    "available_for_investment": 1,
    "min_sip_investment": 1000.0,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 7.3,
    "return1y": -2.16,
    "return5y": null,
    "nav": null,
    "return1d": -0.51,
    "min_investment_amount": 1000.0,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Parag Parikh Long Term Equity Fund Direct Growth",
    "scheme_type": "Growth",
    "fund_manager": "Raunak Onkar, Rajeev Thakkar, Raj Mehta",
    "fund_house": "PPFAS Mutual Fund",
    "scheme_code": "122639",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": true,
    "plan_type": "Direct",
    "page_view": 19102,
    "direct_fund": "PPFAS Mutual Fund",
    "amc": "PPFAS",
    "enable": null,
    "direct_search_id": "parag-parikh-long-term-value-fund-direct-growth",
    "direct_scheme_name": "Parag Parikh Long Term Equity Fund",
    "term_page_view": 1481,
    "logo_url": "https://groww.in/mf-assets/logos/ppfas_groww.png"
}, {
    "id": "axis-long-term-equity-fund-direct-growth",
    "fund_name": "Axis Long Term Equity Fund",
    "search_id": "axis-long-term-equity-fund-direct-growth",
    "category": "Equity",
    "sub_category": "ELSS",
    "sub_sub_category": "",
    "aum": 19632.0086,
    "available_for_investment": 1,
    "min_sip_investment": 500.0,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 4.77,
    "return1y": -11.02,
    "return5y": null,
    "nav": null,
    "return1d": -1.05,
    "min_investment_amount": 500.0,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Axis Long Term Equity Direct Plan Growth",
    "scheme_type": "Growth",
    "fund_manager": "Jinesh Gopani",
    "fund_house": "Axis Mutual Fund",
    "scheme_code": "120503",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 30889,
    "direct_fund": "Axis Mutual Fund",
    "amc": "AXIS",
    "enable": null,
    "direct_search_id": "axis-long-term-equity-fund-direct-growth",
    "direct_scheme_name": "Axis Long Term Equity Fund",
    "term_page_view": 1040,
    "logo_url": "https://groww.in/mf-assets/logos/axis_groww.png"
}, {
    "id": "mirae-asset-emerging-bluechip-fund-direct-growth",
    "fund_name": "Mirae Asset Emerging Bluechip Fund",
    "search_id": "mirae-asset-emerging-bluechip-fund-direct-growth",
    "category": "Equity",
    "sub_category": "Large & Mid Cap",
    "sub_sub_category": "",
    "aum": 8838.97584765,
    "available_for_investment": 1,
    "min_sip_investment": 1000.0,
    "sip_allowed": true,
    "lumpsum_allowed": false,
    "return3y": 1.25,
    "return1y": -14.13,
    "return5y": null,
    "nav": null,
    "return1d": .4,
    "min_investment_amount": 100.0,
    "groww_rating": 5,
    "risk_rating": 4,
    "scheme_name": "Mirae Asset Emerging Bluechip Fund Direct Growth",
    "scheme_type": "Growth",
    "fund_manager": "Neelesh Surana",
    "fund_house": "Mirae Asset Mutual Fund",
    "scheme_code": "118834",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 90789,
    "direct_fund": "Mirae Asset Mutual Fund",
    "amc": "MIRAE_ASSET",
    "enable": null,
    "direct_search_id": "mirae-asset-emerging-bluechip-fund-direct-growth",
    "direct_scheme_name": "Mirae Asset Emerging Bluechip Fund",
    "term_page_view": 740,
    "logo_url": "https://groww.in/mf-assets/logos/mirae_groww.png"
}, {
    "id": "reliance-g-sec-fund-direct-growth",
    "fund_name": "Nippon India Gilt Securities Fund",
    "search_id": "reliance-g-sec-fund-direct-growth",
    "category": "Debt",
    "sub_category": "Gilt",
    "sub_sub_category": "Long Term",
    "aum": 1249.6014,
    "available_for_investment": 1,
    "min_sip_investment": 100.0,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": 10.99,
    "return1y": 17.61,
    "return5y": null,
    "nav": null,
    "return1d": 0.5,
    "min_investment_amount": 5000.0,
    "groww_rating": 5,
    "risk_rating": 3,
    "scheme_name": "Nippon India Gilt Securities Fund Direct Growth",
    "scheme_type": "Growth",
    "fund_manager": "Prashant R.Pimple",
    "fund_house": "Reliance Mutual Fund",
    "scheme_code": "118672",
    "launch_date": null,
    "risk": "Moderate",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 69732,
    "direct_fund": "Reliance Mutual Fund",
    "amc": "RELIANCE",
    "enable": null,
    "direct_search_id": "reliance-g-sec-fund-direct-growth",
    "direct_scheme_name": "Nippon India Gilt Securities Fund",
    "term_page_view": 698,
    "logo_url": "https://groww.in/mf-assets/logos/reliance_groww.png"
}, {
    "id": "uti-nifty-fund-direct-growth",
    "fund_name": "UTI Nifty Index Fund",
    "search_id": "uti-nifty-fund-direct-growth",
    "category": "Others",
    "sub_category": "Index",
    "sub_sub_category": "Nifty",
    "aum": 2097.0756,
    "available_for_investment": 1,
    "min_sip_investment": 500.0,
    "sip_allowed": true,
    "lumpsum_allowed": true,
    "return3y": -0.39,
    "return1y": -22.38,
    "return5y": null,
    "nav": null,
    "return1d": -0.74,
    "min_investment_amount": 5000.0,
    "groww_rating": 3,
    "risk_rating": 4,
    "scheme_name": "UTI Nifty Index Fund Direct Growth",
    "scheme_type": "Growth",
    "fund_manager": "Kaushik Basu",
    "fund_house": "UTI Mutual Fund",
    "scheme_code": "120716",
    "launch_date": null,
    "risk": "Moderately High",
    "doc_type": "scheme",
    "registrar_agent": null,
    "doc_required": false,
    "plan_type": "Direct",
    "page_view": 13923,
    "direct_fund": "UTI Mutual Fund",
    "amc": "UTI",
    "enable": null,
    "direct_search_id": "uti-nifty-fund-direct-growth",
    "direct_scheme_name": "UTI Nifty Index Fund",
    "term_page_view": 652,
    "logo_url": "https://groww.in/mf-assets/logos/uti_groww.png"
}]