export const data = [
    {name: "Services", value: 8.2},
    {name:"Others", value: 11.04},
    {name: "Technology", value: 13.19},
    {name: "Financial", value: 36.16},
    {name: "Healthcare", value: 9.89},
    {name: "FMCG", value: 10.46},
    {name: "Chemicals", value: 6},
    {name: "Communication", value: 5.06}
]


export const debtData = [
    {name: "Others", value: 17.3},
    {name:"Automobile", value: 5.0},
    {name: "Energy", value: 16.8},
    {name: "Technology", value: 14.5},
    {name: "Healthcare", value: 36.2},
    {name: "FMCG", value: 10.2}
]


export const holdingData = [
    {name: "Equity", value: 96.2},
    {name:"Cash", value: 3.8}
]