import React from 'react'
import { PieChart, Pie, Tooltip, Cell } from 'recharts';
//import { data } from '../mockData/pieData'

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#6DA34D', '#8884d8', '#FF9B71', '#E84855', '#548687', '#D56AA0'];
const RADIAN = Math.PI / 180;

const onPieEnter = () => {
    console.log('onPieEnter')
}

const PieChartContainer = ({title, data}) => {
	
  	return (
        <div className='row bg-white rounded'>
            <h4 className='p-3 col-12 border-bottom'>{title}</h4>
            <div className='col'>
                {data && <PieChart width={300} height={400} onMouseEnter={onPieEnter}>
                    <Pie
                        data={data} 
                        cx={140} 
                        cy={180} 
                        innerRadius={50}
                        outerRadius={90} 
                        fill="#8884d8"
                        paddingAngle={5}
                        dataKey='value'
                        label>
                        
                        {data.map((entry, i) => <Cell key={i} fill={COLORS[i % COLORS.length]}/>)}
                    </Pie>
                    <Tooltip />
                </PieChart> }
            </div>
            <div className='col text-left'>
                <ul className="list-group list-group-flush">
                    { data.map((c, i) => <li key={i} className="list-group-item border-0 bullets p-2"> 
                        <span className="material-icons mr-2 " style={{color: COLORS[i]}}> lens </span> 
                        {data[i].name} : {data[i].value} %</li>) }
                </ul>
            </div>
        </div>
    	
    )
}

export default PieChartContainer