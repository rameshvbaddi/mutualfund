import React, { useEffect, useState } from 'react'
import { LineChart, Line, CartesianAxis, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import {chartData} from '../mockData/chartData'

const LineChartComp = ({lineChartData}) => {
    const [width, setWidth] = useState(550)

    useEffect(() => {
        const {offsetWidth} = document.getElementsByClassName('chartConatiner')[0]
        setWidth(offsetWidth)
    },[])
  
    return (
        <>
            <div className='row mb-4 pb-3 border-bottom'>
                <div className='col-1 bg-white'>
                    <img src={lineChartData.logo_url} height='50' width='50'></img>
                </div>
                <div className='col-9 bg-white p-0'>
                    <h4 className='mt-2'>{lineChartData.fund_name}</h4>
                </div>
                <div className='col bg-white'>
                    <div className='price'><span>{lineChartData.aum && lineChartData.aum.toFixed(2)}</span>
                    {lineChartData.return1d > 0 
                        ? <span className="material-icons ml-1 text-success">trending_up</span>
                        : <span className="material-icons ml-1 text-danger">trending_down</span>
                    }
                    </div>
                    
                </div>
            </div>
            <LineChart width={width} height={300} data={chartData} margin={ {right: 100 }} >
                <Line type="monotone" dataKey="value" stroke="#8884d8" />
                <CartesianAxis y={2} stroke="#ccc" />
                <XAxis dataKey="year" />
                <YAxis />
                <Tooltip />
            </LineChart>
        </>
    )
}


export default LineChartComp