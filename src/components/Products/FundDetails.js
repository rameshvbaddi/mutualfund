import React from 'react'

const FundDetails = () => {
    return (
        <div className='col-12 p-5 bgBlue'>
            <h5>Axis Bluechip Fund Direct Plan Growth Details</h5>
            <p>Axis Bluechip Fund Direct Plan Growth is a Equity Mutual Fund Scheme launched by Axis Mutual Fund. This scheme was made available to investors on 01 Jan 2013. Shreyash Devalkar is the Current Fund Manager of Axis Bluechip Fund Direct Plan Growth fund.The fund currently has an Asset Under Management(AUM) of ₹12,717 Cr and the Latest NAV as of 22 May 2020 is ₹28.79.</p>

            <p>The Axis Bluechip Fund Direct Plan Growth is rated Moderately High risk. Minimum SIP Investment is set to 500. Minimum Lumpsum Investment is 5000. For units in excess of 10% of the investment,1% will be charged for redemption within 12 months</p>

            <p>
                <h5>Investment Objective</h5>
                To achieve long term capital appreciation by investing in a diversified portfolio predominantly consisting of equity and equity related securities including derivatives. Howerver, there can be no assurance that the investment objective of the scheme will be achieved.
                Tax Implications</p>
                <p>Returns are taxed at 15%, if you redeem before one year. After 1 year, you are required to pay LTCG tax of 10% on returns of Rs 1 lakh+ in a financial year.</p>
        </div>
    )
}


export default FundDetails