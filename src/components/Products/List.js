import React, { useContext } from 'react'
import './style.css'
import { AppContext } from '../../AppContext/AppContext'

const List = ({data}) => {
    const {goToDetials} = useContext(AppContext)

    

    return (
        <>
            {
                data.map((item, i) => {
                    return <div onClick={() => goToDetials(item)} key={i} className={'listItem conatiner-fluid lightShadow rounded mb-3 ' + (item.return1d > 0 ? 'borderLGreen' : 'borderLRed')}>
                        <div className={'row pb-3 pt-3 '}>
                            <div className='col-2 p-2'>
                                <img src={item.logo_url} height='40px' width='40px' title={item.direct_fund} alt={item.direct_fund} />
                            </div>
                            <div className='col-7'>
                                <div className='row'>
                                    {item.fund_name} 

                                    <span className={'ml-4 ' + (item.return1d > 0 ? 'text-success' : 'text-danger')}><b>{item.return1d}</b></span>
                                    {item.return1d > 0 
                                        ? <span className="material-icons ml-1 text-success">trending_up</span>
                                        : <span className="material-icons ml-1 text-danger">trending_down</span>
                                    }
                                   

                                </div>
                                <div className='row text-secondary'>
                                    <span className='mr-2'><small><span className='text-info'>Fund Manager</span> : {item.fund_manager} | </small></span>
                                    <span className='mr-2'><small><span className='text-info'>Risk Rating</span> : {item.risk_rating} | </small></span>
                                    <span className='mr-2'><small><span className='text-info'>Growth </span>: {item.groww_rating}</small></span>
                                </div>
                            </div>
                            <div className='col pt-2'>
                               <h4 className='font-weight-light mt-1 border-left'>Rs. {item.aum.toFixed(2)}</h4>
                            </div>
                        </div>
                    </div>
                })
            }  
        </>
    )
}

export default List
