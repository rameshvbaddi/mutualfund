import React, {createContext, useState} from 'react'
import { useHistory } from "react-router-dom";
import { listData } from '../components/Products/mockData/listData'

export const AppContext = createContext();

const AppContextProvider = ({children}) => {
    
    const history = useHistory()
    const [isAuth, setisAuth] = useState(false)
    const [lineChartData, setLineChartData] = useState(listData[0])
    const [user, setUser] = useState({})
    const [search, setSearch] = useState('')
    const [filteredData, setFilteredData] = useState([])

    const authUser = (isAuth) => {
        setisAuth(isAuth)
        history.push('/profile')
    }

    const goToSignup = () => {
        setisAuth(false)
        history.push('/')
    }

    const goToProfile = () => {
        history.push('/profile')
    }

    const goToList = () => {
        history.push('/products')
    }

    const goToDetials = (data) => {
        history.push('/productDetails')
        setLineChartData(data)
    }

    const updateUser = (id, val) => {
        setUser({
            ...user,
            [id]: val
        })
    }

    const handleSearch = (val) => {
        setSearch(val)

    }

    return <AppContext.Provider value={{isAuth, authUser, goToSignup, goToProfile ,goToList, goToDetials, lineChartData, user, updateUser, search, handleSearch}}>
        {children}
    </AppContext.Provider>
}

export default AppContextProvider