import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";

import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Header from './components/Header/Header';
import AppRoutes from './components/AppRoutes/AppRoutes';

function App() {
	const { pathname } = useLocation()

	const [bgColor, setbgColor] = useState({background: '#f4f4f4'})

	useEffect(() => {
		pathname !== '/' ? setbgColor({background: '#f4f4f4'}):  setbgColor({background: '#5762D5'})
	}, [pathname])

	return (
		<div className="App" style={bgColor}>
			<Header />
			<AppRoutes />
		</div>
	);
}

export default App;
